<?php 

    include_once './Usuario.php';
    include_once './crudLogin.php';

    $crud = new crudLogin();
    
    if(isset($_REQUEST['cadastrar'])){
        
        $nome =  $_REQUEST['nome'];
        $idade =  $_REQUEST['idade'];
        $sexo =  $_REQUEST['sexo'];
        $user =  $_REQUEST['user'];
        $senha =  $_REQUEST['senha'];
        
        if (!empty($nome)&&!empty($idade)&&!empty($sexo)&&!empty($user)&&!empty($senha)) {
            
            $usuario = new Usuario($nome, $idade, $sexo, $user, $senha);
            $cadastrar = $crud->cadastro($usuario);
            if($cadastrar){
                echo "<script>alert('Usuario Cadastrado com sucesso!');window.location='index.php'</script>";
            }  else {
                echo 'Erro ao efectuar o cadastro!'; 
            }
        }
        else {
               echo 'Existem campos vazios!';
        }
    }
    
?>
