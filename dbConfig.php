<?php

class dbConfig{
    private $dsn;
    private $user;
    private $password;
    private static $pdo = null;
            
    function __construct() {
        $this->dsn = "mysql:host=localhost;dbname=test";
        $this->user = "root";
        $this->password = "";
    }
    
    public function conexao() {
        
        if (is_null(self::$pdo)) {
            try {
                self::$pdo = new PDO($this->dsn, $this->user, $this->password, array(PDO::ATTR_PERSISTENT => TRUE));
                self::$pdo->setAttribute(PDO::ATTR_ERRMODE,  PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $exc) {
                return $exc->getMessage();
            }
        }
        return self::$pdo;
    }
}


?>
