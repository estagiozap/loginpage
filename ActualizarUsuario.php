<html>
  <head>
    <title>Pagina inicial SiGeCELL_ISPTEC</title>
    <meta charsert="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
  </head>
  <body> 
      <?php

        include_once './crudLogin.php';
        $crud = new crudLogin();

        if(isset($_GET['Identificador'])){
            $id = $_GET['Identificador'];
            
            if($crud->actualizarUsuario($id)){
                echo 'sucesso!';
            }  

        }else{
               echo "Id Inexistente!";
        }
    ?>
        <div class="container">
                <div class="row">
                                <div class="col-md-12 alert alert-info">
                                </div>
                </div>
        </div>
        <div class="container">

                <div class="row">
                        <div class="col-md-6 well">

                                <div class="panel panel-default">
                                        <div class="panel-heading">
                                        <h3 class="panel-title">Inserir Usuario:</h3>
                                        </div>

                                        <div class="panel-body">
                                            <form action="editarUsuario.php" method="post">
                                                       
                                                        <div class="form-group">
                                                                <label for="nome">Nome:</label>
                                                                <input type="text" class="form-control" name="nome" id="nome" />
                                                        </div>

                                                        <div class="form-group">
                                                                <label for="idade">Idade:</label>
                                                                <input type="number" class="form-control" name="idade" id="idade" min="0" max="100"></input>
                                                        </div>
                                                                                                                
                                                        <div class="form-group">
                                                            <label for="sexo" >Sexo:</label>
                                                            <div class="form-control">
                                                                <input type="radio" name="sexo" value="Masculino" checked="yes"/>Masculino
                                                                <input type="radio" name="sexo" value="Femenino"/>Femenino
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="user">Username:</label>
                                                                <input type="text" class="form-control" name="user" id="user" />
                                                        </div>
                                                        <div class="form-group">
                                                                <label for="senha">Password:</label>
                                                                <input type="password" class="form-control" name="senha" id="senha" />
                                                        </div>                                                    
                                                        
                                                        <div 
                                                            class="col-md-3"><input class="btn btn-primary" type="submit" name="cadastrar" value="Cadastrar">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <a href="index.php" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Voltar</a>
                                                        </div>
                                                </form>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
    </body>
</html>

