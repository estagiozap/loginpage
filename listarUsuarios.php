<html>
  <head>
    <title>Pagina inicial SiGeCELL_ISPTEC</title>
    <meta charsert="utf-8" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
  </head>
  <body>
      <?php
        include_once './crudLogin.php';
        $crud = new crudLogin();
        $resultado = $crud->listarUsuario();
      ?>
        <div class="container">
                <h3>Listando Usuarios:</h3>
                <div class="row">
                        <div class="col-md-12">
                                <table class="table table-bordered">
                                        <thead>
                                                <tr>
                                                        <th>id_Usuario</th>
                                                        <th>Nome</th>
                                                        <th>Idade</th>
                                                        <th>Sexo</th>
                                                        <th>User</th>
                                                        <th>Password</th>
                                                        <th colspan="1">Accoes</th>
                                                </tr>
                                        </thead>

                                        <tbody>
                                                <?php foreach ($resultado as $dados):?>
                                                <tr>
                                                    <td><?php echo $dados["id_usuario"];?></td>
                                                    <td><?php echo $dados["nome"];?></td>
                                                    <td><?php echo $dados["idade"];?></td>
                                                    <td><?php echo $dados["sexo"];?></td>
                                                    <td><?php echo $dados["user"];?></td>
                                                    <td><?php echo $dados["password"];?></td>
                                                    <td><a href="EliminarUsuario.php?Identificador=<?php echo $dados["id_usuario"];?>" class="btn btn-danger"><span class="glyphicon glyphicon-plus"></span> Eliminar</a></td>
                                                    <!--<td><a href="ActualizarUsuario.php?Identificador=<?php echo $dados["id_usuario"];?>" value="actualizar" class="btn btn-warning"><span class="glyphicon glyphicon-plus"></span> Actualizar</a></td>-->
                                                </tr>
                                            <?php endforeach;?>

                                        </tbody>

                                        <tfoot>

                                        </tfoot>
                                </table>
                            <a href="index.php" class="btn btn-success"><span class="glyphicon glyphicon-backward"></span> Voltar</a>
                        </div>
                </div>
        </div>
    </body>
</html>