<?php
include_once './dbConfig.php';
class crudLogin{
    private $bd;
            
    function __construct() {
        $this->bd = new dbConfig();
    }
    
    public function cadastro($dadosCadastro) {
        
        try {
                $sql = "INSERT INTO tb_usuario(nome,idade,sexo,user,password) VALUES(:nome,:idade,:sexo,:user,:password)";
                $inserir = $this->bd->conexao()->prepare($sql);
                $inserir->bindParam(":nome",$dadosCadastro->getNome());
                $inserir->bindParam(":idade",$dadosCadastro->getIdade());
                $inserir->bindParam(":sexo",$dadosCadastro->getSexo());
                $inserir->bindParam(":user",$dadosCadastro->getUser());
                $inserir->bindParam(":password",$dadosCadastro->getPassword());
                $inserir->execute();
                return TRUE;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function logar($user,$senha) {
        
        try {
                $sql = "SELECT *FROM tb_usuario WHERE user =:user AND password =:password";
                $buscar = $this->bd->conexao()->prepare($sql);
                $buscar->bindParam(":user",$user);
                $buscar->bindParam(":password",$senha);
                $buscar->execute();
                return $buscar->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function listarUsuario() {
        try {
            
                $sql = "SELECT * FROM tb_usuario";
                $listar = $this->bd->conexao()->prepare($sql);
                $listar->execute();
                return $listar->fetchAll();
                
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function eliminarUsuario($id) {
        try {
                $sql = "DELETE FROM tb_usuario WHERE id_usuario = :id";
                $deletar = $this->bd->conexao()->prepare($sql);
                $deletar->bindParam(":id",$id);
                $deletar->execute();
                return TRUE;
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
    
    public function actualizarUsuario($dadosCadastro) {
        
        try {
                
              /*$sql = "UPDATE tb_usuario SET nome=:nome,idade=:idade,sexo=:sexo,user=:sexo,password=:password WHERE id_usuario=:id";
                $actualizar = $this->bd->conexao()->prepare($sql);
                $actualizar->bindParam(":nome",$dadosUsuario->getNome());
                $actualizar->bindParam(":idade",$dadosUsuario->getIdade());
                $actualizar->bindParam(":sexo",$dadosUsuario->getSexo());
                $actualizar->bindParam(":user",$dadosUsuario->getUser());
                $actualizar->bindParam(":password",$dadosUsuario->getPassword());
                $actualizar->bindParam(":id",$dadosUsuario->getId());
                $actualizar->execute();
                return TRUE;*/
        } catch (PDOException $exc) {
            echo $exc->getMessage();
        }
    }
}

?>